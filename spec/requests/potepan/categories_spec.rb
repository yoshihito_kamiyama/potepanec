require 'rails_helper'

RSpec.describe '商品カテゴリーページ', type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon1) { create(:taxon, name: "Bags", taxonomy: taxonomy) }
  let(:taxon2) { create(:taxon, name: "Mugs", taxonomy: taxonomy) }
  let!(:product1) { create(:product, name: "test bag", taxons: [taxon1]) }
  let!(:product2) { create(:product, name: "test mug", taxons: [taxon2]) }

  describe '商品カテゴリー' do
    before { get potepan_category_path(taxon1.id) }

    it '正常なレスポンスが返ってくる' do
      expect(response).to be_successful
    end

    it '適切なタイトルが表示されていること' do
      expect(response.body).to include full_title(taxon1.name)
    end

    it 'カテゴリー分類名が表示されていること' do
      expect(response.body).to include taxonomy.name
    end

    it 'カテゴリー名が表示されていること' do
      expect(response.body).to include taxon1.name
      expect(response.body).to include taxon2.name
    end

    it 'カテゴリー名に紐づいた商品の名前が表示されていること' do
      expect(response.body).to include product1.name
    end

    it 'カテゴリー名に紐づいた商品の価格が表示されていること' do
      expect(response.body).to include product1.price.to_s
    end

    it '指定したカテゴリーに紐づいていない商品が含まれていないこと' do
      expect(response.body).not_to include product2.name
    end
  end
end
