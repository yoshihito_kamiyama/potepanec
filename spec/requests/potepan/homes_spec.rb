require 'rails_helper'

RSpec.describe "ホームページ", type: :request do
  let!(:new_product) { create(:product, name: "test bag", price: 12.34, available_on: Date.today) }
  let!(:old_product) { create(:product, name: "test mug", price: 56.78, available_on: 1.month.ago) }
  let!(:new_products) { create_list(:product, 9, available_on: 1.weeks.ago) }

  before { get potepan_root_path }

  it "正常なレスポンスが返ってくること" do
    expect(response).to be_successful
  end

  describe "新着商品" do
    context "新着商品がある場合" do
      it "新着商品の名前が表示されている" do
        expect(response.body).to include new_product.name
      end

      it '新着商品の価格が表示されている' do
        expect(response.body).to include new_product.display_price.to_s
      end

      it "3週間より前の商品の名前が表示されていない" do
        expect(response.body).not_to include old_product.name
      end

      it "3週間より前の商品の価格が表示されていない" do
        expect(response.body).not_to include old_product.display_price.to_s
      end
    end
  end
end
