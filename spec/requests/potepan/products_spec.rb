require 'rails_helper'

RSpec.describe "商品", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:other_taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:other_product) { create(:product, name: "test mug", taxons: [other_taxon]) }
  let!(:related_products) { create_list(:product, 5, price: 56.78, taxons: [taxon]) }

  describe "商品詳細ページ" do
    before { get potepan_product_path(product.id) }

    it "正常なレスポンスが返ってくること" do
      expect(response).to be_successful
    end

    it "商品の名前が表示されていること" do
      expect(response.body).to include product.name
    end

    it "商品の価格が表示されていること" do
      expect(response.body).to include product.display_price.to_s
    end

    it "商品の説明が表示されていること" do
      expect(response.body).to include product.description
    end

    it "適切なタイトルが表示されていること" do
      expect(response.body).to include full_title(product.name)
    end

    describe "関連商品コンテンツ" do
      context "関連商品がある場合" do
        it "関連した商品が表示されていること" do
          related_products.each do |related_product|
            expect(response.body).to include related_product.display_price.to_s
          end
        end

        it "最大4件までしか表示されていないこと" do
          expect(response.body).not_to include related_products.last.name
        end
      end

      it "関連していない商品が表示されていないこと" do
        expect(response.body).not_to include other_product.name
      end
    end
  end
end
