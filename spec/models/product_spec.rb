require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:other_taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:other_product) { create(:product, taxons: [other_taxon]) }
  let!(:related_product) { create_list(:product, 4, taxons: [taxon]) }

  describe "#related_products" do
    it "呼び出した商品自体は含まれない" do
      expect(product.related_products).not_to include product
    end

    context "呼び出した商品と同じtaxonに属する商品が存在する場合" do
      it "同じtaxonに属する商品を返す" do
        expect(product.related_products).to eq related_product
      end
    end

    context "呼び出した商品と同じtaxonに属する商品が存在しない場合" do
      it "空の配列を返す" do
        expect(other_product.related_products).to eq []
      end
    end
  end
end
