require 'rails_helper'

RSpec.describe "ホームページ", type: :feature do
  let!(:new_products) { create_list(:product, 9, available_on: Date.today) }
  let!(:old_product) { create(:product, available_on: 1.month.ago) }

  describe "新着商品" do
    before { visit potepan_root_path }

    it "3週間より前の商品は表示されていない" do
      expect(page).not_to have_content old_product.name
    end

    it '新着順に表示されている' do
      first(".slide") do
        expect(page).to have_content new_products[8].name
      end
    end

    context "新着商品が8個以上ある場合" do
      it "最大8個を上限として表示されている" do
        expect(page).to have_content new_products[8].name
        expect(page).to have_content new_products[7].name
        expect(page).to have_content new_products[6].name
        expect(page).to have_content new_products[5].name
        expect(page).to have_content new_products[4].name
        expect(page).to have_content new_products[3].name
        expect(page).to have_content new_products[2].name
        expect(page).to have_content new_products[1].name

        expect(page).not_to have_content new_products[0].name
      end
    end
  end
end
