require 'rails_helper'

RSpec.describe "商品詳細ページ", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:other_taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, name: "test bag", price: 12.34, taxons: [taxon]) }
  let!(:other_product) { create(:product, name: "test mug", price: 56.78, taxons: [other_taxon]) }
  let!(:related_products) { create(:product, price: 12.34, taxons: [taxon]) }
  let!(:non_related_products) { create(:product, price: 56.78, taxons: [other_taxon]) }

  describe "商品詳細" do
    before { visit potepan_product_path(product.id) }

    it "idに紐づいた商品が表示されていること" do
      expect(page).to have_title full_title(product.name)
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
    end

    it "idに紐づいていない商品が表示されていないこと" do
      expect(page).not_to have_content other_product.name
      expect(page).not_to have_content other_product.display_price
    end

    it "商品詳細ページからホームページに移動できること" do
      within(".breadcrumb") do
        click_on "Home"
        expect(current_path).to eq potepan_root_path
      end
    end

    it "一覧へ戻るからカテゴリーページに移動できること" do
      within(".media-body") do
        click_on "一覧ページへ戻る"
        expect(current_path).to eq potepan_category_path(taxon.id)
      end
    end

    describe "関連商品コンテンツ" do
      it "商品に関連した商品が表示されていること" do
        within(".productBox") do
          expect(page).to have_content related_products.name
          expect(page).to have_content related_products.display_price
        end
      end

      it "商品に関連していない商品が表示されていないこと" do
        within(".productBox") do
          expect(page).not_to have_content non_related_products.name
          expect(page).not_to have_content non_related_products.display_price
        end
      end

      it "関連商品をクリックすることでその商品の詳細ページに移動できること" do
        within(".productBox") do
          click_on related_products.name
          expect(current_path).to eq potepan_product_path(related_products.id)
        end
      end
    end
  end
end
