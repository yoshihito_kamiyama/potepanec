require 'rails_helper'

RSpec.feature "商品カテゴリーページ", type: :feature do
  let(:taxonomy) { create(:taxonomy, name: "Categories") }
  let(:taxons) { taxonomy.root.children.create(name: "Categories") }
  let(:taxon1) { create(:taxon, name: "Bags", taxonomy: taxonomy, parent_id: taxons.id) }
  let(:taxon2) { create(:taxon, name: "Mugs", taxonomy: taxonomy, parent_id: taxons.id) }
  let!(:product1) { create(:product, name: "test bag", price: "12.34", taxons: [taxon1]) }
  let!(:product2) { create(:product, name: "test mug", price: "56.78", taxons: [taxon2]) }

  describe '商品カテゴリー' do
    before { visit potepan_category_path(taxon1.id) }

    it 'カテゴリーに紐づいた情報が取得できていること' do
      expect(page).to have_title full_title(taxon1.name)
      expect(page).to have_content "Bags"
      expect(page).to have_content "Mugs"
      expect(page).to have_content "test bag"
      expect(page).to have_content "12.34"
      expect(page).not_to have_content "test mug"
      expect(page).not_to have_content "56.78"
    end

    it '商品カテゴリーから他のカテゴリーにページ移動できること' do
      click_on "Categories"
      click_on "Mugs"
      expect(current_path).to eq potepan_category_path(taxon2.id)
      expect(page).to have_content "test mug"
      expect(page).to have_content "56.78"
      expect(page).not_to have_content "test bag"
      expect(page).not_to have_content "12.34"
    end

    it '商品情報から商品詳細ページにページ移動できること' do
      click_on product1.name
      expect(current_path).to eq potepan_product_path(product1.id)
      expect(page).to have_content product1.name
      expect(page).to have_content product1.display_price
      expect(page).to have_content "一覧ページへ戻る"
    end

    it '商品詳細ページからカテゴリーページに移動できること' do
      click_on product1.name
      expect(current_path).to eq potepan_product_path(product1.id)
      click_on '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path(taxon1.id)
    end

    it 'カテゴリーページからホームページにページ移動できること' do
      within(".breadcrumb") do
        click_on "Home"
        expect(current_path).to eq potepan_root_path
      end
      expect(page).to have_content "人気カテゴリー", "新着商品"
    end
  end
end
