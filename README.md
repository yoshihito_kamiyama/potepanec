# 【概要】
  - 名称：ポテパンキャンプECサイト
  - 目的：solidusを用いた商品の表示方法やカテゴリー分類などのECサイトの基本について学んだアプリケーションです。
  - url: https://k-potepanec.herokuapp.com/products/1

## □ 【実装した機能一覧】
- 商品詳細画面(画像表示、商品名、価格、詳細な説明など)

![](docs/images/installation/sample_product.png)

- 商品カテゴリー分類(solidusの初期のTaxonomy、Taxonに基づいた分類)

![](docs/images/installation/sample_category.png)

- 商品詳細における関連商品表示

![](docs/images/installation/sample_related_product.png)

## □ 【使用した技術】
- リンター
    - rubocop-airbnb
- ECパッケージ
    - Solidus

## □ 開発環境等
- 言語:Ruby2.5.1
- フレームワーク:Ruby on Rails
- CSSフレームワーク:bootstrap
- データベース:MySQL
- テスト環境:RSpec(ModelSpec, RequestSpec, Systemspecを記述)
- 本番環境:Heroku
- 画像アップロード（本番環境）:Amazon S3
- リポジトリ管理：Github
- 環境：Docker
