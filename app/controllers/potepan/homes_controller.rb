class Potepan::HomesController < ApplicationController
  MAX_NUMBER_OF_AVAILABLE_PRODUCTS = 8
  def index
    @new_products = Spree::Product.all.where('available_on > ?', 3.week.ago).
      includes(master: [:default_price, :images]).order(available_on: :desc, id: :desc).limit(MAX_NUMBER_OF_AVAILABLE_PRODUCTS)
  end
end
