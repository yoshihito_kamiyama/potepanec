module ApplicationHelper
  def full_title(page_title)
    base_title = "BIGBAG Store"
    if page_title.blank?
      base_title
    else
      "#{page_title} - #{base_title}"
    end
  end

  def display_image(product)
    product.images.first ? product.images.first.url(:product) : Spree::Image.new.url(:product)
  end
end
